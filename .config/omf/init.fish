# directory length
set -g fish_prompt_pwd_dir_length 1
set -g theme_project_dir_length 1

# color
set -g theme_color_scheme dark

# font
set -g theme_avoid_ambiguous_glyphs yes
set -g theme_nerd_fonts no
set -g theme_powerline_fonts yes

# datetime
set -g theme_display_date yes
set -g theme_date_format "+%H:%M:%S"
set -g theme_display_cmd_duration yes

# exit status
set -g theme_show_exit_status yes

# control version system
set -g theme_display_git yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_untracked yes
set -g theme_git_worktree_support yes
set -g theme_display_hg yes

# environment
set -g theme_display_docker_machine yes
set -g theme_display_ruby yes
set -g theme_display_user yes
set -g theme_display_vagrant yes
set -g theme_display_vi yes
set -g theme_display_virtualenv yes

# title
set -g theme_title_display_process yes
set -g theme_title_display_path yes
set -g theme_title_use_abbreviated_path no
